import * as Tripetto from "tripetto-collector";
import { ICheckbox } from "tripetto-block-checkbox";
import "./conditions";

@Tripetto.node("tripetto-block-checkbox")
export class Checkbox extends Tripetto.NodeBlock<HTMLElement, ICheckbox> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("checked");
        const value = this.DataAssert<boolean>(instance, slot);
        const group = document.createElement("div");
        const field = document.createElement("div");
        const table = document.createElement("table");
        const checkbox = document.createElement("div");
        const label = document.createElement("label");
        const input = document.createElement("input");
        const span = document.createElement("span");

        group.className = "form-group";
        field.className = "col-sm-12";
        table.className = "table table-striped table-options";
        checkbox.className = "checkbox";

        input.setAttribute("type", "checkbox");
        span.textContent = this.Node.Props.Name + " ";

        if (slot.Required) {
            const asterisk = document.createElement("span");

            asterisk.className = "form-required";
            asterisk.textContent = "* ";

            span.appendChild(asterisk);
        }

        if (Tripetto.F.IsFilledString(this.Node.Props.Explanation)) {
            const popover = document.createElement("span");

            popover.className = "fa fa-info-circle form-hint";
            popover.setAttribute("role", "button");
            popover.setAttribute("data-toggle", "popover");
            popover.setAttribute("data-placement", "top");
            popover.setAttribute("title", "More information");
            popover.setAttribute("data-content", this.Node.Props.Explanation);

            span.appendChild(popover);

            Tripetto.F.ScheduleAction(() => {
                new Function("", `$(".form-hint").popover();`)();
            });
        }

        input.checked = value.Value;

        input.addEventListener("propertychange", () => (value.Value = input.checked));
        input.addEventListener("change", () => (value.Value = input.checked));
        input.addEventListener("click", () => (value.Value = input.checked));

        label.appendChild(input);
        label.appendChild(span);
        checkbox.appendChild(label);
        table
            .appendChild(document.createElement("tbody"))
            .appendChild(document.createElement("tr"))
            .appendChild(document.createElement("td"))
            .appendChild(checkbox);
        field.appendChild(table);
        group.appendChild(field);

        return group;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("checked");
        const value = this.DataAssert<boolean>(instance, slot);

        return !slot.Required || value.Value;
    }
}
